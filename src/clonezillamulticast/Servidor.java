/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clonezillamulticast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sandra
 */
public class Servidor extends Thread {

    private InetAddress ia;
    private int port;
    private DatagramPacket packet;
    private MulticastSocket socket;
    private byte[] missatge;

    public Servidor(InetAddress ia, int port) {
        this.ia = ia;
        this.port = port;
    }

    public void run() {
        try {
            //el servidor espera escoltant peticións a veure quants es conecten
            System.out.println(Thread.currentThread().getName() + " esperant 10 segons a que els clients es connecten...");
            Thread.sleep(10000);
            socket = new MulticastSocket(port);

            for (int i = 10; i <= 100; i += 10) {

                String texto = Integer.toString(i) + "%";
                missatge = texto.getBytes();

                packet = new DatagramPacket(missatge, missatge.length, ia, port);
                
                String enviament = new String(missatge);
                System.out.println("Servidor emitint...." + enviament);
                socket.send(packet);
                Thread.sleep(2000);
            }
            //quant acaba de emitir, tanca el socket
            System.out.println("Emissió finalitzada");
            socket.close();

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
