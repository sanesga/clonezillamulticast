/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clonezillamulticast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sandra
 */
public class Client extends Thread {

    private InetAddress ia;
    private int port;
    private DatagramPacket packet;
    private MulticastSocket socket;
    private byte[] resposta = new byte[1024];
    private int recibit = 0;
    private int contador = 0;
    private String missatge;

    public Client(InetAddress ia, int port) {
        this.ia = ia;
        this.port = port;
    }

    public void run() {

        try {
            System.out.println(Thread.currentThread().getName() + " CONNECTAT");
            //RECIBEIX RESPOSTA
            socket = new MulticastSocket(port);

            socket.joinGroup(ia);

            packet = new DatagramPacket(resposta, resposta.length, ia, port);

            do {
                socket.receive(packet);

                missatge = new String(resposta);

                System.out.println("\t" + Thread.currentThread().getName() + " recibint resposta..." + missatge);

                //mentres el paquet no envie el missatge de 100% que indica el final, que vaja recibint
            } while (!missatge.contains("100%"));
           
            System.out.println(Thread.currentThread().getName() +" Dades rebudes correctament. Cancel.lant subscripció...");
            socket.close();
            
        } catch (SocketException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
