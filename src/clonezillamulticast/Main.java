/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clonezillamulticast;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Sandra
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, UnknownHostException {

        int port = 4444;
        Servidor s;
        Client c1, c2, c3;
        String ip = "235.1.1.1";
        byte[] arrayBytes = new byte[4];
        String[] arrayIp;
        InetAddress ia;

        //----------PASEM LA IP A ARRAY DE BYTES-----------
        arrayIp = ip.split("\\.");

        int numero1 = Integer.parseInt(arrayIp[0]);
        int numero2 = Integer.parseInt(arrayIp[1]);
        int numero3 = Integer.parseInt(arrayIp[2]);
        int numero4 = Integer.parseInt(arrayIp[3]);

        arrayBytes[0] = (byte) numero1;
        arrayBytes[1] = (byte) numero2;
        arrayBytes[2] = (byte) numero3;
        arrayBytes[3] = (byte) numero4;

        //--------------CREEM LA INETADDRESS----------------
        ia = InetAddress.getByAddress(arrayBytes);

        //----PRIMER EL SERVIDOR ESCOLTA PETICIONS i en el run fa una espera de 10sg---
        s = new Servidor(ia, port);
        s.setName("Servidor");
        System.out.println("Iniciem el servidor..");
        s.start();

        //MENTRES ELS CLIENTS LLANCEN PETICIONS
        c1 = new Client(ia, port);
        c1.setName("Client 1");
        c2 = new Client(ia, port);
        c2.setName("Client 2");
        c3 = new Client(ia, port);
        c3.setName("Client 3");

        //ELS DOS PRIMERS CLIENTS ES CONECTEN A L'HORA I EL TERCER FA TARD
        Thread.sleep(2000);
        c1.start();
        Thread.sleep(6000);
        c2.start();
        Thread.sleep(10100);
        c3.start();
        
        
        s.join();
        c1.join();
        c2.join();
        c3.join();
        
        
    }
}
